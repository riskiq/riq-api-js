# RiskIQ API for node.js

Simple JavaScript interface for the RiskIQ v.1 API, as documented at https://sf.riskiq.net/crawlview/api/docs/.  

## Installation

Installation is currently only available via npm using git.  

```javascript
npm install git+https://bitbucket.org/cs_riskiq/riq-api-js.git
```

## Usage

Usage example:

```javascript
var riq = require('riq-api-js');
var Settings = riq.Settings,
    Whois = riq.Whois;
    
var settings = new Settings(API_TOKEN, API_KEY, API_URL_ROOT);

var whois = new Whois( settings );

whois.query( { domain: 'riskiq.com' }, 100, function (err, res) { 
    if ( err ) {
        console.log("Query failed!");
    } else { 
        if ( res.results < 1 ) {
            console.log("No results.");
        } else {
            console.log(res.domains);
        }
    }
}
```
