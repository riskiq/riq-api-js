/**
 * Created by justinferguson on 10/2/15.
 */

var expect = require('chai').expect,
    sinon = require('sinon'),
    mockery = require('mockery');

// Hard-coded settings object
var settings = require('../test/test.config.js').settings;

var util = require('util');

describe('RiskIQApiProject', function() {
    before( function() {
        mockery.enable();
    });

    beforeEach(function() {
        mockery.registerAllowables( ['superagent','superagent-logger','has-ansi','strip-ansi','ansi-regex','supports-color'] );
        mockery.registerAllowables( ['url','path', 'isarray', 'buffer','events','stream','debug','tty','ms','os','fs','util','http','https' ] );
        mockery.registerAllowables( ['../settings.js','../project.js'] );
        mockery.registerAllowable('./util.js');
    });

    afterEach(function() {
        mockery.deregisterAll();
    });

    after(function() {
        mockery.disable();
    });

    describe("getProjects", function() {
        it('should list the projects in a given workspace', function(done) {

            var Project = require('../lib/project.js')
            var project = new Project(settings);

            var fn = function () {
                project.getProjects( function (err, res) { 
                    if (err) {
                        throw err;
                    }

                    expect(res).to.not.equal(null);
                    console.log(util.inspect(res));
                    done();
                });
            };

            expect(fn).to.not.throw(Error);

        });
    });
});