/**
 * Created by justinferguson on 03/06/2016
 */

var expect = require('chai').expect,
    sinon = require('sinon'),
    mockery = require('mockery');

var util = require('util');

// Hard-coded settings object
var settings = require('../test/test.config.js').settings;

describe('RiskIQApiInventory', function() {
    var inventory;

    before( function() {
        mockery.enable();
    });

    beforeEach(function() {
        mockery.registerAllowables( ['superagent','superagent-logger','has-ansi','strip-ansi','ansi-regex','supports-color', 'async'] );
        mockery.registerAllowables( ['url','path', 'isarray', 'buffer','events','stream','debug','tty','ms','os','fs','util','http','https' ] );
        mockery.registerAllowables( ['../lib/settings.js', '../lib/util.js', '../lib/inventory.js', './util.js' ] );

        var Inventory = require('../lib/inventory.js');

        inventory = new Inventory(settings);
    });

    afterEach(function() {
        mockery.deregisterAll();
    });

    after(function() {
        mockery.disable();
    });

    describe("facets", function() {
        this.timeout(150000);
        var facetlist;
        it('should return a list of possible facets for searching', function(done) {
            var fn = function () {
                inventory.facets( function( err, res ) {
                    if ( err ) {
                        throw err;
                    }
                    expect(res.facet).to.not.be.null;
                    expect(res.facet.length).to.be.within(0,1000);

                    facetlist = res.facet;

                    done();

                });
            };
            expect(fn).to.not.throw(Error);
        });

        it('should return a list of possible values for a single facet', function(done) {
            var fn = function() {
                inventory.facet( facetlist[0], function( err, res ) {
                    if ( err ) {
                        throw err;
                    }

                    //expect(res)

                    console.log( res );

                    done();

                });
            };

            expect(fn).to.not.throw(Error);
        });
    });

    describe("fields", function() {
        this.timeout(150000);
        it ( 'should return a list of possible fields for searching', function(done) {
            var fn = function() {
                inventory.searchFields( function( err, res ) {
                    if ( err ) {
                        throw err;
                    } else {
                        console.log( res );
                    }

                    done();
                });
            };
            expect(fn).to.not.throw(Error);
        });
    });

    describe("tags", function () {
        this.timeout(150000);
        it( 'should return a list of possible tags for searching', function (done ) {
            var fn = function () {
                inventory.tags( function (err, res) {
                    if ( err ) {
                        throw err;
                    } else {
                        console.log(res);
                    }

                    done();

                });
            };

            expect(fn).to.not.throw(Error);
        });
    });

    describe("search", function() {
        this.timeout(150000);
        it('should return all inventory entries created after last week', function(done) {
            var fn = function () {

                // JavaScript Dates give me a sad.
                var date = new Date(new Date().setDate(new Date().getDate()-7));

                inventory.search( { "query": "", "filters": [ { "filters": [ { "field": "assetType", "value": "WEB_SITE", "type": "EQ" } ] } ] },
                    
                    function( err, res ) {
                        if ( err ) {
                            throw err;
                        }

                        console.log( res.inventoryAsset.length + " assets found." );

                    done();

                });
            };

            expect(fn).to.not.throw(Error);
        });
    });

    describe("searchStreaming", function() {
        this.timeout(150000);

        it('should stream all inventory entries created after last week', function(done) {
            var fn = function () {

                var counter = 0;

                var date = new Date(new Date().setDate(new Date().getDate()-30));

                var results = [];

                inventory.searchStreaming( { "query": "", "filters": [ { "filters": [ { "field": "assetType", "value": "WEB_SITE", "type": "EQ" } ] } ] },

                    // Streaming function.
                    function ( ev, cb ) {
                        counter = counter + 1;
                        cb(null);
                    },

                    function ( err, res ) {
                        if ( err ) {
                            throw err;
                        }
                        console.log("Streamed " + counter + " results." );
                        done();
                    }
                );

            };

            expect(fn).to.not.throw(Error);
        });
    })
});