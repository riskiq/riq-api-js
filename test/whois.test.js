/**
 * Created by justinferguson on 10/2/15.
 */

var expect = require('chai').expect,
    sinon = require('sinon'),
    mockery = require('mockery');

// Hard-coded settings object
var settings = require('../test/test.config.js').settings;

describe('RiskIQApiWhois', function() {
    before( function() {
        mockery.enable();
    });

    beforeEach(function() {
        mockery.registerAllowables( ['superagent','superagent-logger','has-ansi','strip-ansi','ansi-regex','supports-color'] );
        mockery.registerAllowables( ['url','path', 'isarray', 'buffer','events','stream','debug','tty','ms','os','fs','util','http','https' ] );
        mockery.registerAllowables( ['../settings.js','../whois.js'] );
        mockery.registerAllowable('./util.js');
    });

    afterEach(function() {
        mockery.deregisterAll();
    });

    after(function() {
        mockery.disable();
    });

    describe("query", function() {
        it('should perform a whois query based on domain name', function(done) {
            this.timeout(15000);

            var Whois = require('../lib/whois.js')
            var whois = new Whois(settings);

            var fn = function () {
                whois.query( { domain: "riskiq.com" }, 250, function (err, res) {
                    if (err) {
                        throw err;
                    }

                    expect(res.results).to.equal(1);
                    expect(res.domains[0].domain).to.equal('riskiq.com');
                    done();
                });
            };

            expect(fn).to.not.throw(Error);

        });
    });
});