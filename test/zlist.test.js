/**
 * Created by justinferguson on 03/06/2016
 */

var expect = require('chai').expect,
    sinon = require('sinon'),
    mockery = require('mockery');

// Hard-coded settings object
var settings = require('../test/test.config.js').settings;

describe('RiskIQApiZList', function() {
    var zlist;

    before( function() {
        mockery.enable();
    });

    beforeEach(function() {
        mockery.registerAllowables( ['superagent','superagent-logger','has-ansi','strip-ansi','ansi-regex','supports-color'] );
        mockery.registerAllowables( ['url','path', 'isarray', 'buffer','events','stream','debug','tty','ms','os','fs','util','http','https' ] );
        mockery.registerAllowables( ['../lib/settings.js', '../lib/util.js', '../lib/zlist.js', './util.js' ] );

        var ZList = require('../lib/zlist.js');

        zlist = new ZList(settings);
    });

    afterEach(function() {
        mockery.deregisterAll();
    });

    after(function() {
        mockery.disable();
    });

    describe("urls", function() {
        it('should return all zlist entries in a given time window', function(done) {
            var fn = function () {
                zlist.urls( new Date("2015-11-13T04:00:00.000-0800"),
                            new Date("2015-11-13T04:30:00.000-0800"),
                            function( err, res ) {
                                if ( err ) {
                                    throw err;
                                }

                                console.log( res );

                                done();

                            });
            };

            expect(fn).to.not.throw(Error);
        });
    });
});