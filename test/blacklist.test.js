/**
 * Created by justinferguson on 10/2/15.
 */

var expect = require('chai').expect,
    sinon = require('sinon'),
    mockery = require('mockery');

// Hard-coded settings object
var settings = require('../test/test.config.js').settings;

var Blacklist = require('../lib/blacklist.js');
var blacklist;

describe('RiskIQApiBlacklist', function() {
    before( function() {
        mockery.enable();
    });

    beforeEach(function() {
        mockery.registerAllowables( ['superagent','superagent-logger','has-ansi','strip-ansi','ansi-regex','supports-color'] );
        mockery.registerAllowables( ['url','path', 'isarray', 'buffer','events','stream','debug','tty','ms','os','fs','util','http','https' ] );
        mockery.registerAllowables( ['../settings.js','../whois.js'] );
        mockery.registerAllowable('./util.js');

        blacklist = new Blacklist(settings);
    });

    afterEach(function() {
        mockery.deregisterAll();
    });

    after(function() {
        mockery.disable();
    });

    describe("lookup", function() {
        it('should perform a blacklist lookup of a url', function(done) {
            var fn = function () {
                blacklist.lookup( "http://eufuu.cambridge.email/", function (err, res) {
                    if (err) {
                        throw err;
                    }

                    console.log( res );

                    expect(res.url).to.equal('http://eufuu.cambridge.email/');
                    done();
                });
            };

            expect(fn).to.not.throw(Error);
        });

        it('should fail cleanly if a URL is not contained in the blacklist', function(done) {
            var fn = function () {
                blacklist.lookup( "http://www.riskiq.net/", function (err, res) {
                    if (err) {
                        throw err;
                    }

                    console.log( res );

                    expect(typeof res.url).to.equal('undefined');
                    done();
                })
            };

            expect(fn).to.not.throw(Error);
        });
    });

    describe("incidentList", function() {
        it('should return a list of incidents from the past day', function(done) {
            var fn = function () {
                blacklist.incidentList( null, null, true, function (err, res) {
                   if (err) {
                       throw err;
                   }

                    console.log( res );

                    done();
                })
            };

            expect(fn).to.not.throw(Error);
        });

        // TODO more testcases
    });

    describe("listAll", function () {
        // TODO
    });

    describe("exploitBinary", function() {
        // TODO
    });

    describe("incident", function () {
        // TODO
    });

});