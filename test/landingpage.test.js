/**
 * Created by justinferguson on 11/13/15.
 */

var expect = require('chai').expect,
    sinon = require('sinon'),
    mockery = require('mockery');

var util = require('util');

// Hard-coded settings object
var settings = require('../test/test.config.js').settings;

describe('RiskIQApiLandingPage', function () {
    var landingpage;

    before(function () {
        mockery.enable();
    });

    beforeEach(function () {
        mockery.registerAllowables(['superagent', 'superagent-logger', 'has-ansi', 'strip-ansi', 'ansi-regex', 'supports-color', 'async']);
        mockery.registerAllowables(['url', 'path', 'isarray', 'buffer', 'events', 'stream', 'debug', 'tty', 'ms', 'os', 'fs', 'util', 'http', 'https']);
        mockery.registerAllowables(['../lib/settings.js', '../lib/util.js', '../lib/landingpage.js', './util.js']);

        var LandingPage = require('../lib/landingpage.js');

        landingpage = new LandingPage(settings);
    });

    afterEach(function () {
        mockery.deregisterAll();
    });

    after(function () {
        mockery.disable();
    });

    describe("submit", function () {
        this.timeout(60000);
        it('should submit a single url as a landing page', function (done) {
            var fn = function () {
                landingpage.submit("http://www.google.com/", function (err, res) {
                    if (err) {
                        throw err;
                    }

                    console.log(util.inspect(res));

                    done();
                });
            };
            expect(fn).to.not.throw(Error);
        });

        it('should submit a single object as a landing page', function (done) {
            var fn = function () {
                landingpage.submit( { url: "http://www.google.com/", keyword: "Testing" },
                function( err, res ) {
                    if (err) {
                        throw err;
                    } else {
                       console.log(util.inspect(res));
                       done();
                    }
                })
            };
            expect(fn).to.not.throw(Error);
        });

        it('should submit multiple urls as landing pages', function (done) {
            var fn = function () {
                landingpage.submitBulk(["http://www.google.com/", "http://www.slashdot.org/"], function (err, res) {
                    if (err) {
                        throw err;
                    } else {
                        console.log(util.inspect(res));
                        expect(res.length).to.be.equal(2);
                        done();
                    }
                });
            };
            expect(fn).to.not.throw(Error);
        });

        it('should submit multiple objects as landing pages', function( done ) {
            var fn = function () {
                landingpage.submitBulk( [ { url: 'http://www.google.com/',
                                            projectName: 'LP - Test - 1234',
                                            md5: '1234567890abcdef',
                                            fields: [] },
                                          { url: 'http://www.slashdot.org/',
                                            projectName: 'LP - Test - 1234',
                                            md5: 'fedcba0987654321',
                                            fields: [] } ],
                /* landingpage.submitBulk( [ { url: "http://www.google.com/", keyword: "Testing " },
                                          { url: "http://www.slashdot.org/", keyword: "Testing" } ],  */
                    function (err, res) {

                    if (err) {
                        throw err;
                    } else {
                        console.log(util.inspect(res));
                        expect(res.length).to.be.equal(2);
                        done();
                    }
                });
            };
            expect(fn).to.not.throw(Error);

        })
    });

    describe("crawled", function () {
        this.timeout(150000);
        it('should return most recent 1000-ish crawls', function (done) {
            var fn = function () {
                landingpage.crawled(null,
                    null,
                    null,
                    function (err, res) {
                        if (err) {
                            throw err;
                        }

                        console.log("Got " + res.results + " results.");

                        expect(res.results).to.be.within(0, 1100);
                        done();
                    });
            };

            expect(fn).to.not.throw(Error);
        });

        it('should return all crawls in a given time window', function (done) {
            var fn = function () {
                landingpage.crawled(new Date(new Date().setDate(new Date().getDate() - 13)),
                    new Date(new Date().setDate(new Date().getDate() - 9)),
                    false, function (err, res) {
                        if (err) {
                            throw err;
                        }

                        // console.log( res );

                        done();

                    });
            };

            expect(fn).to.not.throw(Error);
        });
        /*
         it('should fail cleanly if a URL is not contained in the blacklist', function(done) {
         var fn = function () {
         blacklist.lookup( "http://www.riskiq.net/", function (err, res) {
         if (err) {
         throw err;
         }

         console.log( res );

         expect(typeof res.url).to.equal('undefined');
         done();
         })
         };

         expect(fn).to.not.throw(Error);
         }); */
    });
});