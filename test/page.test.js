/**
 * Created by justinferguson on 10/2/15.
 */

var expect = require('chai').expect,
    sinon = require('sinon'),
    mockery = require('mockery'),
    util = require('util');

// Hard-coded settings object
var settings = require('../test/test.config.js').settings;

describe('RiskIQApiPage', function() {
    before( function() {
        mockery.enable();
    });

    beforeEach(function() {
        mockery.registerAllowables( ['superagent','superagent-logger','has-ansi','strip-ansi','ansi-regex','supports-color'] );
        mockery.registerAllowables( ['url','path', 'isarray', 'buffer','events','stream','debug','tty','ms','os','fs','util','http','https' ] );
        mockery.registerAllowables( ['../settings.js','../page.js'] );
        mockery.registerAllowable('./util.js');
    });

    afterEach(function() {
        mockery.deregisterAll();
    });

    after(function() {
        mockery.disable();
    });

    describe("query", function() {
        it('should perform a page lookup of a specific page', function(done) {
            this.timeout(15000);

            var Page = require('../lib/page.js')
            var page = new Page(settings);

            var fn = function () {
                page.page( "9aa4e361-4d20-411f-b885-c826a1034ed6", "381f5c80-de46-4468-8560-4c8eca7c9ce8", function (err, res) {
                    if (err) {
                        throw err;
                    }

                    expect(res.startsWith("<page")).to.be.true;
                    done();
                });
            };

            expect(fn).to.not.throw(Error);

        });
    });
});