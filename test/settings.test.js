/**
 * Created by jferg@riskiq.net on 10/2/15.
 */

var expect = require('chai').expect,
    sinon = require('sinon'),
    mockery = require('mockery');

const API_TOKEN = "123456789";
const API_KEY = "abcdefghijklmnopqrstuvwxyz";
const API_URL_ROOT = "https://riq-api-url/test/v1/";

describe('RiskIQApiSettings', function() {
    before( function() {
        mockery.enable();
    });

    beforeEach(function() {
        mockery.registerAllowable('superagent');
        mockery.registerAllowable('../lib/settings.js');
    });

    afterEach(function() {
        mockery.deregisterAll();
    });

    after(function() {
        mockery.disable();
    });

    describe("create", function() {
        it('should create a new settings object with empty settings', function() {
            var Settings = require("../lib/settings.js");
            var settings = new Settings();
            expect(settings.api_key).to.equal(null);
            expect(settings.api_token).to.equal(null);
            expect(settings.api_url_root).to.equal(null);
        });

        it('should create a new settings object with only a URL set', function() {
            var Settings = require("../lib/settings.js");
            var settings = new Settings( {'api_url_root': API_URL_ROOT} );
            expect(settings.api_key).to.equal(null);
            expect(settings.api_token).to.equal(null);
            expect(settings.api_url_root).to.equal(API_URL_ROOT);
        });

        it('should create a new settings object with api_key and api_token set', function() {
            var Settings = require("../lib/settings.js");
            var settings = new Settings( {'api_token': API_TOKEN, 'api_key': API_KEY} );
            expect(settings.api_token).to.equal(API_TOKEN);
            expect(settings.api_key).to.equal(API_KEY);
            expect(settings.api_url_root).to.equal(null);
        });

        it('should create a new settings object with all values set', function() {
            var Settings = require("../lib/settings.js");
            var settings = new Settings( {'api_token': API_TOKEN, 'api_key': API_KEY, 'api_url_root': API_URL_ROOT} );
            expect(settings.api_token).to.equal(API_TOKEN);
            expect(settings.api_key).to.equal(API_KEY);
            expect(settings.api_url_root).to.equal(API_URL_ROOT);
        });
    });
});