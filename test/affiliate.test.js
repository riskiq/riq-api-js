/**
 * Created by justinferguson on 10/2/15.
 */

var expect = require('chai').expect,
    sinon = require('sinon'),
    mockery = require('mockery');

var util = require('util');

// Hard-coded settings object
var settings = require('../test/test.config.js').settings;

var Affiliate = require('../lib/affiliate.js');
var affiliate;

describe('RiskIQApiAffiliate', function() {
    before( function() {
        mockery.enable();
    });

    beforeEach(function() {
        mockery.registerAllowables( ['superagent','superagent-logger','has-ansi','strip-ansi','ansi-regex','supports-color'] );
        mockery.registerAllowables( ['url','path', 'isarray', 'buffer','events','stream','debug','tty','ms','os','fs','util','http','https' ] );
        mockery.registerAllowables( ['../settings.js','../affiliate.js', '../whois.js', '../page.js'] );
        mockery.registerAllowable('./util.js');

        affiliate = new Affiliate(settings);
    });

    afterEach(function() {
        mockery.deregisterAll();
    });

    after(function() {
        mockery.disable();
    });

    describe("incidentList", function() {
        it('should return a list of incidents from the past day', function(done) {
            var fn = function () {
                affiliate.incidentList( null, null, null, null, function (err, res) {
                   if (err) {
                       throw err;
                   }

                    // console.log( util.inspect( res, { depth: 5 } ) );

                    done();
                })
            };

            expect(fn).to.not.throw(Error);
        });

        // TODO more testcases
    });

});