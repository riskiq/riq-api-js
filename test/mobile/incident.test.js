/**
* Created by justinferguson on 10/2/15.
*/

var expect = require('chai').expect,
    sinon = require('sinon'),
    mockery = require('mockery');

// Hard-coded settings object
var settings = require('../../test/test.config.js').settings;

describe('RiskIQApiMobileIncident', function() {
    before( function() {
        mockery.enable();
    });

    beforeEach(function() {
        mockery.registerAllowables( ['superagent','superagent-logger','has-ansi','strip-ansi','ansi-regex','supports-color'] );
        mockery.registerAllowables( ['url','path', 'isarray', 'buffer','events','stream','debug','tty','ms','os','fs','util','http','https' ] );
        mockery.registerAllowables( ['../settings.js','../../lib/mobile/incident.js'] );
        mockery.registerAllowable('./util.js');
    });

    afterEach(function() {
        mockery.deregisterAll();
    });

    after(function() {
        mockery.disable();
    });

    describe("list", function() {
        it('should perform a mobile incident list query', function(done) {
            this.timeout(15000);

            var Incident = require('../../lib/mobile/incident.js');
            var incident = new Incident(settings);

            var fn = function () {
                incident.list( null, null, function (err, res) {
                    if (err) {
                        throw err;
                    }

                    expect(res.results).to.equal(0);
                    done();
                });
            };

            expect(fn).to.not.throw(Error);

        });
    });

    describe("get", function() {
        it('should get an incident record by id', function(done) {
            this.timeout(15000);

            var Incident = require('../../lib/mobile/incident.js');
            var incident = new Incident(settings);

            var fn = function () {
                incident.get( 123, function (err, res) {
                    if (err) {
                        throw err;
                    }

                    expect(res.id).to.equal(123);
                    done();
                });
            };

            expect(fn).to.not.throw(Error);

        });
    });
});
