/**
 * Created by justinferguson on 10/2/15.
 */

var expect = require('chai').expect,
    sinon = require('sinon'),
    mockery = require('mockery');

// Hard-coded settings object
var settings = require('../test/test.config.js').settings;

describe('RiskIQApiEvent', function() {
    before( function() {
        mockery.enable();
    });

    beforeEach(function() {
        mockery.registerAllowables( ['superagent','superagent-logger','has-ansi','strip-ansi','ansi-regex','supports-color', 'async'] );
        mockery.registerAllowables( ['url','path', 'isarray', 'buffer','events','stream','debug','tty','ms','os','fs','util','http','https' ] );
        mockery.registerAllowables( ['../settings.js','../lib/event.js'] );
        mockery.registerAllowable('./util.js');
    });

    afterEach(function() {
        mockery.deregisterAll();
    });

    after(function() {
        mockery.disable();
    });

    describe("search", function() {
        it('should perform a event search based on create time', function(done) {
            this.timeout(15000);

            var Event = require('../lib/event.js');
            var event = new Event(settings);

            var fn = function () {
                event.search( { "query": "", "filters": [ { "filters": [ { "field": "createdAt", "value": "2015-03-01", "type": "GTE" } ] } ] }, function (err, res) {
                    if (err) {
                        throw err;
                    }

                    expect(res.totalResults).to.be.greaterThan(1);
                    done();
                });
            };

            expect(fn).to.not.throw(Error);

        });
    });

    describe("searchStreaming", function() {

        it('should stream all events created after 2015-06-01', function(done) {
            
            this.timeout(150000);
            
            var Event = require('../lib/event.js');
            var event = new Event(settings);
            
            var fn = function () {

                var counter = 0;

                var date = new Date(new Date().setDate(new Date().getDate()-30));

                var results = [];

                event.searchStreaming( { "query": "", "filters": [ { "filters": [ { "field": "createdAt", "value": "2016-06-01", "type": "GTE" } ] } ] },

                    // Streaming function.
                    function ( ev, cb ) {
                        counter = counter + 1;
                        console.log("Got event.");
                        cb(null);
                    },

                    function ( err, res ) {
                        if ( err ) {
                            throw err;
                        }
                        console.log("Streamed " + counter + " results." );
                        done();
                    }
                );

            };

            expect(fn).to.not.throw(Error);
        });
    })
});