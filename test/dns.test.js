/**
 * Created by justinferguson on 10/2/15.
 */

var expect = require('chai').expect,
    sinon = require('sinon'),
    mockery = require('mockery');

// Hard-coded settings object
var settings = require('../test/test.config.js').settings;

describe('RiskIQApiDns', function() {
    var dns = null;

    before( function() {
        mockery.enable();
    });

    beforeEach(function() {
        mockery.registerAllowables( ['superagent','superagent-logger','has-ansi','strip-ansi','ansi-regex','supports-color'] );
        mockery.registerAllowables( ['url','path', 'isarray', 'buffer','events','stream','debug','tty','ms','os','fs','util','http','https' ] );
        mockery.registerAllowables( ['../lib/settings.js','../lib/dns.js'] );
        mockery.registerAllowable('./util.js');

        var DNS = require('../lib/dns.js');
        dns = new DNS(settings);
    });

    afterEach(function() {
        mockery.deregisterAll();
    });

    after(function() {
        mockery.disable();
    });

    describe("queryByData", function() {
        it('should perform a query of DNS data by IP address', function(done) {
            var fn = function () {
                dns.byData({ip: "8.8.8.8"}, null, 100, function (err, res) {
                    if (err) {
                        throw err;
                    }

                    expect(res.recordCount).to.be.at.least(1);
                    done();
                });
            };

            expect(fn).to.not.throw(Error);
        });

        it('should perform a query of DNS data by text', function(done) {

            var fn = function () {
                dns.byData({name: "*.riskiq.net"}, null, 100, function (err, res) {
                    if (err) {
                        throw err;
                    }
                    expect(res.recordCount).to.be.at.least(1);
                    done();
                });
            };

            expect(fn).to.not.throw(Error);
        });

        it('should properly handle an empty response', function(done) {

            var fn = function() {
                dns.byData({name: "supercalifragilisticexpialidocious.riskiq.net"}, null, 100, function (err, res) {
                    if (err) {
                        throw err;
                    }
                    expect(res.recordCount).to.equal(0);
                    done();
                });

            };

            expect(fn).to.not.throw(Error);
        });
    });

    describe("queryByName", function() {
        it('should perform a forward DNS query by domain name', function(done) {

            var fn = function () {
                dns.byName( { name: "*.riskiq.com" }, null, 100, function (err, res) {
                    if (err) {
                        throw err;
                    }

                    // expect(res.statusCode).to.equal(200, "Query did not return a 200 status.");
                    expect(res.recordCount).to.be.at.least(1);
                    // expect(res.domains[0].domain).to.equal('riskiq.com');
                    done();
                });
            };

            expect(fn).to.not.throw(Error);

        });

        it ('should perform a reverse DNS query by PTR record', function(done) {
            var fn = function () {
                // Using Google's nameserver as a test address because it -should-
                // almost always be in the PDNS DB.
                dns.byName( { name: "8.8.8.8.in-addr.arpa" }, "PTR", 250, function (err, res) {
                    if ( err) {
                        throw err;
                    }

                    expect(res.recordCount).to.be.at.least(1);
                    done();
                });
            };

            expect(fn).to.not.throw(Error);
        });

        it ('should properly handle an empty response', function(done) {
            var fn = function () {
                dns.byName( { name: "asdfjkl.blahblahblahtestblah.blah" }, "A", 250, function (err, res) {
                    if ( err ) {
                        throw err;
                    }

                    expect(typeof res).to.equal("object");
                    expect(res.recordCount).to.equal(0);
                    done();
                })
            };

            expect(fn).to.not.throw(Error);
        });
    });
});