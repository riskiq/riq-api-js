/**
 * Created by jferg@riskiq.net on 10/2/15.
 */

var expect = require('chai').expect,
    sinon = require('sinon'),
    mockery = require('mockery');


describe('RiskIQApiUtil', function() {
    before( function() {
        mockery.enable();
    });

    beforeEach(function() {
        mockery.registerAllowable('url');
    });

    afterEach(function() {
        mockery.deregisterAll();
    });

    after(function() {
        mockery.disable();
    });

    describe("build_url", function() {
        var utils = require('../lib/util.js');
        it('should build a correct URL from a url and one path element', function() {
            var testurl = utils.build_url( "http://things.and.stuff.com/", "coolstuff" );
            expect(testurl).to.equal("http://things.and.stuff.com/coolstuff");
        });

        it('should build a correct URL from a url and two path elements', function() {
            var testurl = utils.build_url( "http://things.and.stuff.com/", "more", "coolstuff");
            expect(testurl).to.equal("http://things.and.stuff.com/more/coolstuff");
        });

        it('should build a correct URL from a url and many path elements', function() {
            var testurl = utils.build_url( "http://things.and.stuff.com/", "even", "more", "really", "freakin/cool", "stuff");
            expect(testurl).to.equal("http://things.and.stuff.com/even/more/really/freakin/cool/stuff");
        });
    });

    // describe("build_filter", function() {
    //     var utils = require('../lib/util.js');
    //     /* it('should build a correct equality filter', function() {
    //         var testfilter = utils.filterBuilder.eq( "assetType", "WEB_SITE" );
    //         expect(JSON.stringify(testfilter)).to.equal('{"filters":[{"field":"assetType","value":"WEB_SITE","type":"EQ"}]}');
    //     }); */
    //     it('should build a correct equality filter', function() {
    //         // var testfilter = utils.filterBuilder.ne( "assetType", "WEB_SITE" );
    //
    //         /* var testfilter = utils.filterBuilder.filter(field("assetType").equals("WEB_SITE"));
    //             testfilter2 = utils.filterBuilder.filter(field("assetType").equals("WEB_SITE")).or.filter(field("assetType").equals("ASN"));
    //             testfilter3 = utils.filterBuilder.filter(field("assetType").equals("WEB_SITE")).or.filter(field("assetType").equals("SSL_CERT"));
    //             testfilter4 = utils.filterBuilder.filter(testfilter2).and.filter(testfilter3); */
    //
    //         var testfilter = utils.fb.filter(utils.fb.field("assetType").equals("WEB_SITE"));
    //
    //         expect(testfilter.toString()).to.equal('{\"filters\":[{\"filters\":[{\"field\":\"assetType\",\"value\":\"WEB_SITE\",\"type\":\"EQ\"}]}]}');
    //     });
    //
    //     it('should build a correct inequality filter', function() {
    //         var testfilter = utils.fb.filter(utils.fb.field("assetType").not.equals("WEB_SITE"));
    //     })
    // });

    describe("append_query_string", function() {
        var utils = require('../lib/util.js');
        it('should append a correct query string to a simple url', function() {
            var testurl = utils.append_query_parameters("https://thingsandstuff.com/coolstuff", { whichStuff: 1, stuffType: 'good' });
            expect(testurl).to.equal("https://thingsandstuff.com/coolstuff?whichStuff=1&stuffType=good");
        });
        it('should append a query string to a url which already has a query string', function() {
            var testurl = utils.append_query_parameters("https://thingsandstuff.com/coolstuff?reallyCool=true", { whichStuff: 1, stuffType: 'excellent'});
            expect(testurl).to.equal("https://thingsandstuff.com/coolstuff?reallyCool=true&whichStuff=1&stuffType=excellent");
        });
        it('should append a query string to a url which already has a value for values in the query string', function() {
            var testurl = utils.append_query_parameters("https://stuffandsuch.com/neatstuff?somestuff=a", { somestuff: ["b", "c"] , morestuff: "d" });
            expect(testurl).to.equal("https://stuffandsuch.com/neatstuff?somestuff=a&somestuff=b&somestuff=c&morestuff=d");
        })
    })
});