'use strict';

var util = require('./util.js');
var async = require('async');
var nodeutil = require('util');

var EVENT_SEARCH_API_PATH = '/event/search';

module.exports = Event;

/**
 *
 *
 * Class to interact with RiskIQ event API (BETA).  For details on this API, see:
 * https://sf.riskiq.net/crawlview/api/docs/controllers/EventController.html
 *
 * Created by jferg@riskiq.net.
 *
 * @class Event
 * */

/**
 * Instantiate a event API object using the given settings object for configuration.
 *
 * @param {Settings} settings A riq-api-js Settings object configured with the correct URL, api_key, and api_token.
 * @constructor
 */

function Event(settings) {
    this._settings = settings;
};

/**
 * Perform a event search asynchronously against the RiskIQ API.
 *
 * @param {Object} criteria Set of criteria to query for. Currently only allowed criteria is 'createdAt'.
 * @param {Function} callback Callback function.
 * @example event.search( { "query": "", "filters": [ { "filters": [ { "field": "createdAt", "value": "2015-03-01", "type": "GTE" } ] } ] }, function (err, res ) {
 *              if ( err ) {
 *                  console.log("Query failed.");
 *              } else {
 *                  console.log("Results: ");
 *                  console.log(res);
 *              }
 */

Event.prototype.search = function (criteria, callback) {

    var qo = criteria;

    util.postJSONToURL(EVENT_SEARCH_API_PATH, null, qo, this._settings, callback);

};

/**
 * Perform an event search asynchronously against the RiskIQ API, streaming results instead of returning one big object. 
 * @param {Object} criteria Set of criteria to query for.  
 * @param {Function} streamer Function to receive events as they are returned from the API, one event at a time.  
 * @param {Function} callback Callback function.
 * @example event.search( {"query": "", "filters": [ { "filters": [ { "field": "createdAt", "value": "2016-06-01", "type": "GTE" } ] } ] }, 
 *                          function ( event ) {
 *                              console.log( "Got event! " + event.id );
 *                          },
 *                          function ( err, res ) {
 *                            if (err) {
 *                              console.log("Error streaming events.  Last event was at: " + res.lastEvent );
 *                            } else {
 *                              console.log("Done streaming events.  All events matching criteria were streamed.");
 *                            }
 *                          } );
 */

Event.prototype.searchStreaming = function (criteria, streamer, callback) {

    var qo = criteria;

    var qurl = EVENT_SEARCH_API_PATH;

    var results = 500;
    var offset = 0;
    var creslen = -1;

    var rescount = 0;

    var settings = this._settings;

    var lastEvDate = 0;

    function streamEvents(events, seCallback) {
        if (events.length > 0) {
            async.eachLimit(events, 5,
                function (ev, done) {
                    streamer(ev, function () {
                        process.nextTick(done);
                    });

                    rescount = rescount + 1;

                    if (ev.lastChanged) {
                        lastEvDate = ev.lastChanged;
                    } else {
                        lastEvDate = ev.firstSeen;
                    }
                },

                function (err) {
                    seCallback(err);
                }
            );

        } else {
            process.nextTick(seCallback);
        }
    }

    async.whilst(
        function () {
            return (creslen !== 0);
        },

        function (done) {
            util.postJSONToURL(qurl, {results: results, offset: offset}, qo, settings, function (err, res) {
                if (err) {
                    
                    done(err);

                } else {
                    console.log(nodeutil.inspect(res));
                    
                    creslen = res.Results.length;

                    streamEvents(res.Results, function (err, res) {
                        offset = offset + creslen;
                        done(err);
                    });
                }
            });
        },
        function (err) {
            callback(err, { lastEvent: lastEvDate } );
        }
    )
}
