'use strict';

var util = require('./util.js');
var request = require('superagent');
var logger = require('superagent-logger');

var PROJECT_QUERY_API_PATH = '/project/list';
var PROJECT_KEYWORD_QUERY_API_PATH = '/project';

module.exports = Project;

/**
 *
 *
 * Class to interact with RiskIQ project API.  For details on this API, see:
 * https://sf.riskiq.net/crawlview/api/docs/controllers/ProjectController.html
 *
 * Created by jferg@riskiq.net.
 *
 * @class Project
 * */

/**
 * Instantiate a project API object using the given settings object for configuration.
 *
 * @param {Settings} settings A riq-api-js Settings object configured with the correct URL, api_key, and api_token.
 * @constructor
 */

function Project( settings ) {
    this._settings = settings;
};

/**
 * List projects in workspace corresponding to current API key.
 *
 * @param {Function} callback Callback function.
 * @example projects.getProjects( function ( err, res ) {
 *              if ( err ) {
 *                  console.log("Getting projects failed.");
 *              } else {
 *                  console.log("Results: ");
 *                  console.log(res);
 *              }
 */

Project.prototype.getProjects = function ( callback ) {

    util.getURLWithParameters(PROJECT_QUERY_API_PATH, null, this._settings, callback);

}

/**
 * List keywords for specified project. 
 * 
 * @param {Integer} projectId Project ID
 * @param {Function} callback Callback function. 
 * 
 */

Project.prototype.getKeywords = function( projectId, callback ) {
    var qurl = PROJECT_KEYWORD_QUERY_API_PATH + "/" + projectId + "/keywords";
    util.getURLWithParameters( qurl, null, this._settings, callback );
}
