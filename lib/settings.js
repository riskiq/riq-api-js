/**
 * Created by justinferguson on 10/2/15.
 */

var util = require("./util.js");

var defaults = {
    'api_key'      : null,
    'api_token'    : null,
    'api_url_root' : null,
    'debug'        : false,
    'logger'       : util.simple_logger,
    'use_landingpage_paging_workaround': false
};

var Settings = (function () {

    // Constructor.
    function Settings() {

        var options = {};

        Object.defineProperty(this, '__',  // Define property for field values
            {value: {}});

        if (arguments[0])
            options = arguments[0];

        for (var arg in defaults) {
            if (typeof options[arg] === "undefined")
                this[arg] = defaults[arg];
            else
                this[arg] = options[arg];
        }
    };

    (function define_fields(defaults) {
        Object.keys(defaults).forEach(function (field_name) {
            Object.defineProperty(Settings.prototype, field_name, {
                get: function () {
                    return this.__ [field_name];
                },
                set: function (new_value) {
                    this.__[field_name] = new_value;
                }
            });
        });
    })(defaults);

    return Settings;

})();

module.exports = Settings;


