'use strict';

var util = require('./util.js');
var request = require('superagent');
var logger = require('superagent-logger');
var nodeutil = require( 'util' );

var AFFILIATE_INCIDENT_LIST_API_PATH = '/affiliate/incident/list';
var AFFILIATE_CAMPAIGN_SUMMARY_API_PATH = '/affiliate/campaignSummary';


module.exports = Affiliate;

/**
 *
 *
 * Class to interact with RiskIQ affiliate API.  For details on this API, see:
 * https://sf.riskiq.net/crawlview/api/docs/controllers/AffiliateController.html
 *
 * Created by jferg@riskiq.net.
 *
 * @class Affiliate
 * */

/**
 * Instantiate a affiliate API object using the given settings object for configuration.
 *
 * @param {Settings} settings A riq-api-js Settings object configured with the correct URL, api_key, and api_token.
 * @constructor
 */

function Affiliate( settings ) {
    this._settings = settings;
};

/**
 * Return a list of all affiliate incidents in your workspace for the given time period.
 *
 * @param {Date} startDate Start date to search from (inclusive).  If not provided or not valid, defaults to today.
 * @param {Date) endDate End date to end search on (inclusive).  If not provided or not valid, defaults to yesterday.
 * @example blacklist.lookup( "http://www.riskiq.net/", function (err, res ) {
 *              if ( err ) {
 *                  console.log("Query failed.");
 *              } else {
 *                  console.log("Results: ");
 *                  console.log(res);
 *              }
 */

Affiliate.prototype.incidentList = function ( startDate, endDate, knownProfile, maxResults, callback ) {

    if ( !(Object.prototype.toString.call(endDate) === '[object Date]')) { endDate = new Date() };
    if ( !(Object.prototype.toString.call(startDate) === '[object Date]')) { startDate = new Date(); startDate.setDate(endDate.getDate()-1); }

    if ( maxResults === null ) { maxResults = 1000 }
    if ( knownProfile === null ) { knownProfile = false; }

    util.getURLWithParameters( AFFILIATE_INCIDENT_LIST_API_PATH,
        { startDateInclusive: startDate.toISOString(), endDateExclusive: endDate.toISOString(),
            knownProfile: knownProfile, maxResults: maxResults },
        this._settings,
        callback );

};

