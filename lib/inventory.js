'use strict';

var path = require('path');
var util = require('./util.js');
var nodeutil = require('util');
var request = require('superagent');
var logger = require('superagent-logger');
var async = require('async');

var INVENTORY_SEARCH_API_PATH = '/inventory/search';
var INVENTORY_FACETS_API_PATH = '/inventory/facets';
var INVENTORY_SEARCH_FIELDS_API_PATH = '/inventory/search/fields';
var INVENTORY_TAGS_API_PATH = '/inventory/tags';

module.exports = Inventory;

/**
 *
 *
 * Class to interact with RiskIQ inventory API (BETA).  For details on this API, see:
 * https://sf.riskiq.net/crawlview/api/docs/controllers/InventoryController.html
 *
 * Created by jferg@riskiq.net.
 *
 * @class Inventory
 * */

/**
 * Instantiate a inventory API object using the given settings object for configuration.
 *
 * @param {Settings} settings A riq-api-js Settings object configured with the correct URL, api_key, and api_token.
 * @constructor
 */

function Inventory( settings ) {
    this._settings = settings;
};

/**
 * Perform a inventory inventory asynchronously against the RiskIQ API.
 *
 * @param {Object} criteria Set of criteria to query for. Currently only allowed criteria is 'createdAt'.
 * @param {Function} callback Callback function.
 * @example inventory.search( { "query": "", "filters": [ { "filters": [ { "field": "createdAt", "value": "2015-03-01", "type": "GTE" } ] } ] }, function (err, res ) {
 *              if ( err ) {
 *                  console.log("Query failed.");
 *              } else {
 *                  console.log("Results: ");
 *                  console.log(res);
 *              }
 */

Inventory.prototype.search = function ( criteria, callback ) {

    var qo = criteria;

    var qurl = INVENTORY_SEARCH_API_PATH;
    // XXX This will fail if more than 500 results are returned - needs to be made to handle offsets, etc.
    util.postJSONToURL(qurl, { results: 500 }, qo, this._settings, callback);

};

/**
 * Perform an inventory search asynchronously against the RiskIQ API and stream the results back.
 *
 * @param {Object} criteria Set of criteria to query for.  Currently only allowed criteria is 'createdAt'.
 * @param {Function} streamer Streaming function.  This function will be called with each inventory asset, individually,
 *                            until there are no more left, or an error occurs.
 * @param {Function} callback Callback function. This function will be called when the streaming function has been
 *                            called for each asset, or an error occurs.
 */

Inventory.prototype.searchStreaming = function ( criteria, streamer, callback ) {

    var qo = criteria;

    var qurl = INVENTORY_SEARCH_API_PATH;

    var scroll = null;

    var settings = this._settings;

    var lastEvDate = 0;
    var rescount = 0;

    var creslen = -1;   // Length of the current batch of responses.

    function streamEvents( events, seCallback ) {
        if ( events.length > 0 ) {
            async.eachLimit(events, 5,

                function (ev, done) {
                    // Send event to our streaming function.
                    streamer(ev, function( ) {
                        process.nextTick(done);
                    });
                    rescount = rescount + 1;
                    if ( ev.lastChanged ) {
                        lastEvDate = ev.lastChanged;
                    } else {
                        lastEvDate = ev.firstSeen;
                    }
                },

                // Callback from seriesEach.
                function (err) {
                    seCallback(err);
                }
            );
        } else {
            process.nextTick(seCallback);
        }
    }


    async.whilst(
        // Condition.
        function () { return (creslen !== 0); },

        // Action
        function (done) {
            util.postJSONToURL( qurl, { results: 2000, scroll: scroll }, qo, settings, function ( err, res ) {
                    if ( err ) {
                        done( err );
                    } else {

                        creslen = res.inventoryAsset.length;    // Update current response length.

                        scroll = res.scroll;

                        // Send this batch of events to the streaming function.
                        streamEvents( res.inventoryAsset, function ( err, res ) {
                            done(err);
                        });

                    }
                },
                function ( err ) {
                    callback( err );
                }
            )
        },

        // Wrapup
        function ( err ) {

            callback( err, {
                endDateExclusive: new Date(lastEvDate)
            } );
        } );
};

/**
 * Query for valid facets for searching.
 *
 * @param {Function} callback Callback function.
 * @example inventory.facets( function (err, res ) {
 *              if ( err ) {
 *                  console.log("Query failed.");
 *              } else {
 *                  console.log("Results: ");
 *                  console.log(res);
 *              }
 */

Inventory.prototype.facets = function ( callback ) {

    var params = {};

    var settings = this._settings;

    util.getURLWithParameters( INVENTORY_FACETS_API_PATH, params, settings, function ( err, res ) {
        callback( err, res );
    });
};

/**
 * Query for valid facet values for a given facet for searching.
 *
 * @param {Function} callback Callback function.
 * @param {String} facetName
 * @example inventory.facets( function (err, res ) {
 *              if ( err ) {
 *                  console.log("Query failed.");
 *              } else {
 *                  console.log("Results: ");
 *                  console.log(res);
 *              }
 */

Inventory.prototype.facet = function ( facetName, callback ) {

    var params = {};

    var settings = this._settings;

    var qurl = path.join( INVENTORY_FACETS_API_PATH, facetName );

    util.getURLWithParameters( INVENTORY_FACETS_API_PATH, params, settings, function ( err, res ) {
        callback( err, res );
    });
};


/**
 * Query for valid search fields for searching.
 *
 * @param {Function} callback Callback function.
 * @example inventory.searchFields( function (err, res ) {
 *              if ( err ) {
 *                  console.log("Query failed.");
 *              } else {
 *                  console.log("Results: ");
 *                  console.log(res);
 *              }
 */

Inventory.prototype.searchFields = function ( callback ) {

    var params = {};

    var settings = this._settings;

    util.getURLWithParameters( INVENTORY_SEARCH_FIELDS_API_PATH, params, settings, function ( err, res ) {
        callback( err, res );
    });
};

/**
 * Query for valid tags for searching.
 *
 * @param {Function} callback Callback function.
 * @example inventory.tags( function (err, res ) {
 *              if ( err ) {
 *                  console.log("Query failed.");
 *              } else {
 *                  console.log("Results: ");
 *                  console.log(res);
 *              }
 */

Inventory.prototype.tags = function ( callback ) {

    var params = {};

    var settings = this._settings;

    util.getURLWithParameters( INVENTORY_TAGS_API_PATH, params, settings, function ( err, res ) {
        callback( err, res );
    });
};

