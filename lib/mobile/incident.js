'use strict';

var util = require('../util.js');
var path = require('path');
var request = require('superagent');
var logger = require('superagent-logger');

var MOBILE_INCIDENT_LIST_API_PATH = '/mobile/incident/list';
var MOBILE_INCIDENT_GET_API_PATH = '/mobile/incident/get';

module.exports = Incident;

/**
 *
 *
 * Class to interact with RiskIQ Mobile Incident API.  For details on this API, see:
 * https://sf.riskiq.net/crawlview/api/docs/controllers/AppIncidentController.html
 *
 * Created by jferg@riskiq.net.
 *
 * @class Incident
 * */

/**
 * Instantiate a mobile incident API object using the given settings object for configuration.
 *
 * @param {Settings} settings A riq-api-js Settings object configured with the correct URL, api_key, and api_token.
 * @constructor
 */

function Incident( settings ) {
    this._settings = settings;
};

/**
 * Return a list of mobile incidents in the workspace associated with the API token for a given time period.
 * Default is to return past 24 hours.
 *
 * @param {Date} startDate Start date (inclusive).
 * @param {Date} endDate End date (exclusive).
 * @param {Function} callback Callback function.
 * @example incident.list( null, null, function( err, res ) {
 *                  if ( err ) {
 *                      console.log( "Failed to get events for previous day." );
 *                  } else {
 *                      console.log( "Got " + res.results + " events for previous day." );
 *                  }
 */

Incident.prototype.list = function ( startDate, endDate, callback ) {

    if ( typeof startDate === 'undefined' || startDate == null ) {
        startDate = new Date();
    }

    if ( typeof endDate === 'undefined' || endDate == null ) {
        endDate = new Date();
        endDate.setHours(startDate.getHours()+1);
    }

    var qo = {startDateInclusive: startDate.toISOString(), endDateExclusive: endDate.toISOString() };

    util.getURLWithParameters( MOBILE_INCIDENT_LIST_API_PATH,
        qo,
        this._settings,
        callback );
};

/**
 * Return details for a single mobile incident (by ID) in the workspace associated with the given API token.
 *
 * @param {Integer} incidentId
 * @param {Function} callback
 */

Incident.prototype.get = function (incidentId, callback) {

    if ( typeof incidentId === 'undefined' ) callback("No incidentId provided.", null);

    var qpath = path.join(MOBILE_INCIDENT_GET_API_PATH, incidentId.toString() );

    util.getURLWithParameters( qpath, null, this._settings, callback );

}
