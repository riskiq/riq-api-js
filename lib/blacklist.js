'use strict';

var util = require('./util.js');
var request = require('superagent');
var logger = require('superagent-logger');

var BLACKLIST_LOOKUP_API_PATH = '/blacklist/lookup';
var BLACKLIST_INCIDENT_API_PATH = '/blacklist/incident';
var BLACKLIST_INCIDENT_LIST_API_PATH = '/blacklist/incident/list';
var BLACKLIST_LIST_API_PATH = '/blacklist/list';
var BLACKLIST_MALWARE_API_PATH = '/blacklist/malware';
var BLACKLIST_EXPLOITBINARY_API_PATH = '/blacklist/exploitBinary';

module.exports = Blacklist;

/**
 *
 *
 * Class to interact with RiskIQ blacklist API.  For details on this API, see:
 * https://sf.riskiq.net/crawlview/api/docs/controllers/BlacklistController.html
 *
 * Created by jferg@riskiq.net.
 *
 * @class Blacklist
 * */

/**
 * Instantiate a blacklist API object using the given settings object for configuration.
 *
 * @param {Settings} settings A riq-api-js Settings object configured with the correct URL, api_key, and api_token.
 * @constructor
 */

function Blacklist( settings ) {
    this._settings = settings;
};

Blacklist.FilterEnum = {
    BLACKHOLE: 'blackhole',
    SAKURA: 'sakura',
    EXPLOIT_KIT: 'exploitKit'
};


/**
 * Perform a blacklist lookup asynchronously against the RiskIQ API.
 *
 * @param {String} url The URL to search the blacklist for.  "http://" will be assumed if the URL does not
 * begin with it.
 * @param {Function} callback Callback function.
 * @example blacklist.lookup( "http://www.riskiq.net/", function (err, res ) {
 *              if ( err ) {
 *                  console.log("Query failed.");
 *              } else {
 *                  console.log("Results: ");
 *                  console.log(res);
 *              }
 */

Blacklist.prototype.lookup = function ( url, callback ) {

    if ( typeof url === 'undefined' || url === "" ) {
        callback("No url specified.", {});
    }

    util.getURLWithParameters( BLACKLIST_LOOKUP_API_PATH,
                                { url: url },
                                this._settings,
                                callback );

}

/**
 * Return a list of all incidents for the given URL in your workspace, starting at {startIndex}.
 *
 * @param {String} url URL for which to return incidents.
 * @param {Integer} maxResults The maximum number of results to return for one request.  Max is 1000, default is 100.
 * @param {Integer} startIndex The offset at which to start in the full set of results for this URL.  Will return at most
 *                             10000 results.
 * @param {Function} callback Callback function.
 *
 * @example blacklist.lookup( "http://www.riskiq.net/", function (err, res ) {
 *              if ( err ) {
 *                  console.log("Query failed.");
 *              } else {
 *                  console.log("Results: ");
 *                  console.log(res);
 *              }
 */


Blacklist.prototype.incident = function ( url, maxResults, startIndex, callback ) {

    if ( typeof url === 'undefined' || url === "" ) {
        callback("No url specified.", {});
    }

    if ( typeof startIndex === 'undefined' || startIndex == null ) {
        startIndex = 0;
    }

    if ( typeof maxResults === 'undefined' || maxResults == null ) {
        maxResults = 100;
    }

    util.getURLWithParameters( BLACKLIST_INCIDENT_API_PATH,
                                { maxResults: maxResults, startIndex: startIndex, url: url },
                                this._settings,
                                callback );

}

/**
 * Return a list of all incidents in your workspace for the given time period.
 *
 * @param {Date} startDate Start date to search from (inclusive).  If not provided or not valid, defaults to today.
 * @param {Date) endDate End date to end search on (inclusive).  If not provided or not valid, defaults to yesterday.
 * @param {Boolean} allCrawls Return all incidents from any crawls within your workspace (true), or
 *                            return only incidents from crawls which are landing pages, site scans, or
 *                            match a brand classifier (false - default).
 *
 * @example blacklist.lookup( "http://www.riskiq.net/", function (err, res ) {
 *              if ( err ) {
 *                  console.log("Query failed.");
 *              } else {
 *                  console.log("Results: ");
 *                  console.log(res);
 *              }
 */

Blacklist.prototype.incidentList = function ( startDate, endDate, allCrawls, callback ) {

    if ( !(startDate instanceof Date)) startDate = new Date();
    if ( !(endDate instanceof Date)) { endDate = new Date(); endDate.setDate(startDate.getDate()+1); }

    if ( allCrawls == true ) { allCrawls = true; } else { allCrawls = false; }

    util.getURLWithParameters( BLACKLIST_INCIDENT_LIST_API_PATH,
        { startDateInclusive: startDate.toISOString(), endDateExclusive: endDate.toISOString(), allWorkspaceCrawls: allCrawls },
        this._settings,
        callback );

}

/**
 * Return a list of all incidents in your workspace for the given time period.
 *
 * @param {Date} startDate Start date to search from (inclusive).  If not provided or not valid, defaults to one hour ago.
 * @param {Date) endDate End date to end search on (inclusive).  If not provided or not valid, defaults to now.
 * @param {Blacklist.FilterEnum} filter Filter to filter results with.  Currently supported filters are:
 *                                          FilterEnum.BLACKHOLE - Resources flagged as hosting a Black Hole exploit kit.
 *                                          FilterEnum.SAKURA - Resources flagged as hosting a Sakura exploit kit.
 *                                          FilterEnum.EXPLOIT_KIT - Resources flagged as hosting any exploit kit.
 *
 * @example TODO blacklist.lookup( "http://www.riskiq.net/", function (err, res ) {
 *              if ( err ) {
 *                  console.log("Query failed.");
 *              } else {
 *                  console.log("Results: ");
 *                  console.log(res);
 *              }
 */

Blacklist.prototype.listAll = function (startDate, endDate, filter) {
    if ( !(startDate instanceof Date)) startDate = new Date();
    if ( !(endDate instanceof Date)) { endDate = new Date(); endDate.setHours(startDate.getHours()+1); }

    var qo = {startDateInclusive: startDate.toISOString(), endDateExclusive: endDate.toISOString() };

    if ( typeof filter !== 'undefined' ) {
        qo['filter'] = filter;
    }

    util.getURLWithParameters( BLACKLIST_LIST_API_PATH,
        qo,
        this._settings,
        callback );

}

Blacklist.prototype.malware = function () {
    // XXX TODO
}

Blacklist.prototype.exploitBinary = function() {
    // XXX TODO
}


