'use strict';

var util = require('./util.js');
var request = require('superagent');
var logger = require('superagent-logger');
var async = require('async');

var LANDINGPAGE_GET_API_PATH = '/landingPage';
var LANDINGPAGE_VERSIONS_API_PATH = '/landingPage/versions';
var LANDINGPAGE_CREATE_API_PATH = '/landingPage';
var LANDINGPAGE_CRAWLED_API_PATH = '/landingPage/crawled';
var LANDINGPAGE_FLAGGED_API_PATH = '/landingPage/flagged';
var LANDINGPAGE_BULK_GET_API_PATH = '/landingPage/bulk';
var LANDINGPAGE_BULK_CREATE_API_PATH = '/landingPage/bulk';
var LANDINGPAGE_MALICIOUSBINARY_API_PATH = '/landingPage/maliciousBinary';
var LANDINGPAGE_GET_PROJECTS = '/landingPage/projects';

var MAX_BULK_SUBMISSIONS_PER_BATCH = 990;

module.exports = LandingPage;

/**
 *
 *
 * Class to interact with RiskIQ Landing Page API.  For details on this API, see:
 * https://sf.riskiq.net/crawlview/api/docs/controllers/LandingPageController.html
 *
 * Created by jferg@riskiq.net.
 *
 * @class LandingPage
 * */

/**
 * Instantiate a blacklist API object using the given settings object for configuration.
 *
 * @param {Settings} settings A riq-api-js Settings object configured with the correct URL, api_key, and api_token.
 * @constructor
 */

function LandingPage( settings ) {
    this._settings = settings;
};

/*!
 * Private function to check a submitted URL and format properly for submission.
 */

function _checkSubmission(data) {
    var submission = {};

    if ( typeof data === 'string' ) {
        // If it's a string, we're going to assume that it's just a URL and submit that.
        submission.url = data;
    } else {
        if (data.url) {
            submission.url = data.url;
        } else {
            return "Invalid submission - no URL provided.";
        }

        if (data.md5) {
            submission.md5 = data.md5;
        }

        if (data.fields) {
            submission.fields = data.fields;
        }

        if (data.keyword) {
            submission.keyword = data.keyword;
        }

        if (data.projectName) {
            submission.projectName = data.projectName;
        }

        if (data.pingbackUrl) {
            submission.pingbackUrl = data.pingbackUrl;
        }
    }

    return submission;
}
/**
 * Retrieve a single landing page by MD5.
 *
 * @param {String} md5 The MD5 value for the landing page to retrieve.
 * @param {Function} callback Callback function.
 * @example landingPage.get( "4499dd22aa001188", function (err, res ) {
 *              if ( err ) {
 *                  console.log("Query failed.");
 *              } else {
 *                  console.log("Results: ");
 *                  console.log(res);
 *              }
 */

LandingPage.prototype.lookup = function ( md5, callback ) {

    if ( typeof md5 === 'undefined' || md5 === "" ) {
        callback("No md5 specified.", {});
    }

    util.getURLWithParameters( path.join( LANDINGPAGE_GET_API_PATH, md5 ),
        { },
        this._settings,
        callback );

}

/**
 * Submit a single landing page.
 *
 * @param {Object} data The data to submit for the landing page.  See https://sf.riskiq.net/crawlview/api/docs/schemas/V1LandingPageSubmissionRequest.html
 *                      for structure.  
 * @param {Function} callback Callback function.
 * @example landingPage.submit( { url: "https://www.google.com/", 
 *                                md5: "08675309deadbeef", 
 *                                keyword: "The googles they do nothing!" }, 
*                               function( err, res ) {
*                                    if ( err ) {
*                                       console.log("Submission failed.");
*                                    } else {
*                                       console.log("Submission succeeded: " + res);
*                                    } );
 */

LandingPage.prototype.submit = function( data, callback ) {

    var submission = _checkSubmission( data );

    if ( typeof submission === 'string') {
        // If we got a string, it's an error.
        callback( submission );
    } else {
        util.postJSONToURL( LANDINGPAGE_CREATE_API_PATH, {}, submission, this._settings, callback);
    }
    
};

/**
 * Submit multiple landing pages.
 *
 * @param {Array} landingPages Array containing the list of pages to submit.  These can be either strings, or landingpage submission objects.  
 *         Objects and strings (URLs) may be intermixed in the same array.
 *         See https://sf.riskiq.net/crawlview/api/docs/schemas/V1LandingPageSubmissionRequest.html for structure.
 * @param {Function} callback Callback function.
 * @example landingPage.submitBulk( [ "http://www.google.com/", "http://www.slashdot.org/", { url: "http://www.metafilter.com/", projectName: "LP - myProject" } ], 
 *                               function( err, res ) {
 *                                    if ( err ) {
 *                                       console.log("Submission failed.");
 *                                    } else {
 *                                      console.log( res.length + " landingpage submitted." );
 *                                    } );
 */


LandingPage.prototype.submitBulk = function( landingPages, callback ) {

    if ( Array.isArray( landingPages ) ) {
        var submissions = landingPages.map( function(s) { return _checkSubmission(s); });

        var i = 0;

        var results = [];
        
        var settings = this._settings;

        async.whilst (
            
            function() { return i < Math.ceil(submissions.length / MAX_BULK_SUBMISSIONS_PER_BATCH) },
            
            function( cb2 ) {
                
                var batch = { entry: submissions.slice( (i * MAX_BULK_SUBMISSIONS_PER_BATCH), ( (i + 1) * MAX_BULK_SUBMISSIONS_PER_BATCH) - 1) };
                
                i += 1;
                
                util.postJSONToURL(LANDINGPAGE_BULK_CREATE_API_PATH, {}, batch, settings, function(err, res) {
                    
                    if ( ! err ) {
                        results.push.apply(results, res.entry);
                    } 
                    
                    cb2( err );
                });
            },
            function( err, n ) {
                callback( err, results );
            }
        );
        
    } else {
        // Should have been passed an array of strings or objects, but just in case we get something else, we'll
        // just submit it as a single LP (or try to).
        this.submit( landingPages, callback );
    }
}

/**
 * Last landing pages by crawl date.  (ascending)
 *
 * @param {Date} startDate The time to start listing landing pages at.  If not specifed, returns most recent records.
 * @param {Date} endDate The time to end the landing page listing at. If not specified, returns 100 records.
 * @param {boolean} whois Include whois information in results.
 * @param {Function} callback Callback function.
 * @example landingPage.crawl( new Date("2015-11-11T01:01:01"),
 *                             new Date("2015-11-11T01:01:10"), false, function (err, res) {
 *                              if (err) {
 *                                  console.log("Query failed.");
 *                              } else {
 *                                  console.log("Results: " );
 *                                  console.log( util.inspect( res ) );
 *                              } );
 *
 */
LandingPage.prototype.crawled = function (startDate, endDate, whois, callback) {

    if (typeof whois === 'undefined' || whois == null) {
        whois = false;
    }

    var params = {whois: whois};

    if (typeof startDate !== 'undefined' && startDate !== null) {
        params.start = startDate.toISOString();
    }

    if (typeof endDate !== 'undefined' && endDate != null) {
        params.end = endDate.toISOString();
    }

    // console.log( "Querying for crawls: " + startDate + " --> " + endDate );

    var settings = this._settings;

    var endDateExclusive = new Date(0);

    var fullResults;
    
    if ( endDate === null ) {
        util.getURLWithParameters( LANDINGPAGE_CRAWLED_API_PATH, params, settings, callback ); 
    } else {

        async.until(

            function () {
                return endDateExclusive >= endDate
            },

            function (cb) {
                util.getURLWithParameters(LANDINGPAGE_CRAWLED_API_PATH, params, settings, function (err, res) {
                    // If we got an error, return to the callback.
                    // If no end date was specified, return to the callback.

                    if (settings.debug) console.log("Got batch of results!");
                    if (settings.debug) console.log("Count: " + res.results + "   start: " + res.startDateInclusive + "  end: " + res.endDateExclusive);

                    if (err || typeof endDate === 'undefined' || endDate === null) {

                        cb(err, res);

                    } else {

                        if ( !res || res.results === 0) {

                            endDateExclusive = endDate;

                        } else if (res.results === 1100 && settings.use_landingpage_paging_workaround === true) {

                            endDateExclusive = new Date(new Date(res.endDateExclusive).getTime() - 1000);
                            if ( res.landingPage ) {
                                res.landingPage = res.landingPage.filter(function (d) {
                                    return (new Date(d.crawlData.crawl.startDate) < new Date(endDateExclusive));
                                });
                                res.results = res.landingPage.length;
                            } else {
                                res.results = 0;
                                res.landingPage = [];
                            }
                            if (settings.debug) console.log("Had 1100 events, now have " + res.results + " events.");

                        } else {
                            endDateExclusive = new Date(res.endDateExclusive);
                        }

                        if ( ! fullResults ) {
                            if ( res && res.results !== 0 ) {
                                fullResults = res;
                            } else {
                                fullResults = {
                                    landingPage: [],
                                    endDateExclusive: new Date()
                                }
                            }
                        } else {
                            if ( res ) {
                                fullResults.landingPage.push.apply( fullResults.landingPage, res.landingPage );
                                fullResults.endDateExclusive = new Date(res.endDateExclusive);
                            }
                        }

                        params.start = endDateExclusive.toISOString();

                        cb(null);
                    }
                })
            },
            function (err, n) {
                if (settings.debug) console.log( "CrawledLandingPages has " + fullResults.landingPage.length + " entries.");
                callback( err, fullResults );
            });
    }
}

