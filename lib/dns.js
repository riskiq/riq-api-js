'use strict';

var util = require('./util.js');
var request = require('superagent');
var logger = require('superagent-logger');

var DNS_QUERY_BYNAME_API_PATH = "/dns/name/";
var DNS_QUERY_BYDATA_API_PATH = "/dns/data/";

module.exports = DNS;

/**
 * Class to interact with RiskIQ dns API.  For details on this API, see:
 * https://sf.riskiq.net/crawlview/api/docs/controllers/DnsController.html
 *
 * Created by jferg@riskiq.net.
 *
 * @class DNS
 *
 * */

/**
 * Instantiate a DNS API object using the given settings object for configuration.
 *
 * @param {Settings} settings A riq-api-js Settings object configured with the correct URL, api_key, and api_token.
 * @constructor
 */

function DNS( settings ) {
    this._settings = settings;
};

/**
 * Perform a DNS Query by record name asynchronously against the RiskIQ API.
 *
 * @param {Object} criteria Set of criteria to query for.  Only one of 'name', 'ip', or 'raw' may be specified.
 * @param {String} rrType Optional record type to filter by. For example: "A", "MX", "CNAME", etc.
 *                        If an unrecognized rrType is passed an error will be returned.
 * @param {Integer} maxResults Maximum number of results to return.  Capped at 1000.  Default is 100.
 * @param {Function} callback Callback function.
 */

DNS.prototype.byName = function ( criteria, rrType, maxResults, callback ) {
    var qo = criteria;

    var maxResults = typeof maxResults !== 'undefined' ? maxResults : 100;
    qo.maxResults = maxResults;

    util.getURLWithParameters( DNS_QUERY_BYNAME_API_PATH, qo, this._settings, callback );
}

/**
 * Perform a DNS Query by record data asynchronously against the RiskIQ API.
 *
 * @param {Object} criteria Set of criteria to query for.  Only one of 'name', 'ip', or 'raw' may be specified.
 * @param {String} rrType Optional record type to filter by. For example: "A", "MX", "CNAME", etc.
 *                        If an unrecognized rrType is passed an error will be returned.
 * @param {Integer} maxResults Maximum number of results to return.  Capped at 1000.  Default is 100.
 * @param {Function} callback Callback function.
 */

DNS.prototype.byData = function ( criteria, rrType, maxResults, callback ) {
    var qo = criteria;

    var maxResults = typeof maxResults !== 'undefined' ? maxResults : 100;
    qo.maxResults = maxResults;

    if ( typeof rrType !== undefined && rrType != null ) {
        qo.rrType = rrType;
    }

    util.getURLWithParameters( DNS_QUERY_BYDATA_API_PATH, qo, this._settings, callback );
}
