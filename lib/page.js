'use strict';

var util = require('./util.js');
var path = require('path');
var request = require('superagent');
var logger = require('superagent-logger');

var PAGE_API_PATH = '/page';

module.exports = Page;

/**
 *
 *
 * Class to interact with RiskIQ page API.  For details on this API, see:
 * https://sf.riskiq.net/crawlview/api/docs/controllers/WhoisController.html
 *
 * Created by jferg@riskiq.net.
 *
 * @class Page
 * */

/**
 * Instantiate a page API object using the given settings object for configuration.
 *
 * @param {Settings} settings A riq-api-js Settings object configured with the correct URL, api_key, and api_token.
 * @constructor
 */

function Page( settings ) {
    this._settings = settings;
};

/**
 * Perform a page retrieval asynchronously against the RiskIQ API.  Returns page(s) in RIQ native XML format.
 *
 * @param {String} crawlGuid GUID of the crawl containing the page to retrieve.
 * @param {String} pageGuid GUID of the page to retrieve.
 * @param {Function} callback Callback function.
 * @example page.page( "12345678-1234-5678-123456", "98765432-9876-0987-654321", function (err, res ) {
 *              if ( err ) {
 *                  console.log("Query failed.");
 *              } else {
 *                  console.log("Results: ");
 *                  console.log(res);
 *              }
 */

Page.prototype.page = function ( crawlGuid, pageGuid, callback ) {

    var qurl = path.join(PAGE_API_PATH, crawlGuid, pageGuid );

    util.getXMLURLWithParameters( qurl, null, this._settings, callback );

}
