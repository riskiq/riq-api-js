/*
 riskiq filterbuilder
 
 Copyright(c) 2016 Justin Ferguson/RiskIQ <jferg@riskiq.net>
 
 Some code shamelessly stolen from chai.js Assertions.
 
 MIT Licensed
 */

module.exports = function (_riq, util) {
    
    /*
     * Module Dependencies.
     */
    var flag = util.flag;
    
    /*
     * Module export.
     */
    _riq.FilterBuilder = FilterBuilder;
    
    function FilterBuilder ( obj ) {
        flag( this, 'object', obj );
    } 
    
    FilterBuilder.addProperty = function( name, fn ) {
        util.addProperty(this.prototype, name, fn );
    };
    
    FilterBuilder.addMethod = function ( name, fn ) {
        util.addMethod( this.prototype, name, fn );
    }
    
    FilterBuilder.addChainableMethod = function ( name, fn, chainingBehavior ) {
        util.addChainableMethod(this.prototype, name, fn, chainingBehavior );
    }
    
    FilterBuilder.overwriteProperty = function (name, fn) {
        util.overwriteProperty(this.prototype, name, fn);
    };

    FilterBuilder.overwriteMethod = function (name, fn) {
        util.overwriteMethod(this.prototype, name, fn);
    };

    FilterBuilder.overwriteChainableMethod = function (name, fn, chainingBehavior) {
        util.overwriteChainableMethod(this.prototype, name, fn, chainingBehavior);
    };


}