'use strict';

var util = require('./util.js');
var request = require('superagent');
var logger = require('superagent-logger');

var ZLIST_QUERY_API_PATH = '/zlist/urls';

module.exports = ZList;

/**
 *
 *
 * Class to interact with RiskIQ zlist API.  For details on this API, see:
 * https://sf.riskiq.net/crawlview/api/docs/controllers/ZListController.html
 *
 * Created by jferg@riskiq.net.
 *
 * @class ZList
 * */

/**
 * Instantiate a zlist API object using the given settings object for configuration.
 *
 * @param {Settings} settings A riq-api-js Settings object configured with the correct URL, api_key, and api_token.
 * @constructor
 */

function ZList( settings ) {
    this._settings = settings;
};

/**
 * Query for ZList entries added to the ZList during a given time period.
 *
 * @param {Date} startDate Time to start returning ZList entries at.  (Inclusive)
 * @param {Date} endDate Time to end returning ZList entries at. (Exclusive)
 * @param {Function} callback Callback function.
 * @example zlist.urls( new Date(now), new Date(now), function (err, res ) {
 *              if ( err ) {
 *                  console.log("Query failed.");
 *              } else {
 *                  console.log("Results: ");
 *                  console.log(res);
 *              }
 */

ZList.prototype.urls = function ( startDate, endDate, callback ) {

    var params = {};

    if ( typeof startDate !== 'undefined' && startDate !== null ) {
        params.start = startDate.toISOString();
    }

    if ( typeof endDate !== 'undefined' && endDate != null ) {
        params.end = endDate.toISOString();
    }

    var settings = this._settings;

    util.getURLWithParameters( ZLIST_QUERY_API_PATH, params, settings, function ( err, res ) {
        callback( err, res );
    });
};

/**
 * Query for ZList entries added to the ZList during a given time period.
 *
 * @param {Date} startDate Time to start returning ZList entries at.  (Inclusive)
 * @param {Date} endDate Time to end returning ZList entries at. (Exclusive)
 * @param {Function} callback Callback function.
 * @example zlist.urls( new Date(now), new Date(now), function (err, res ) {
 *              if ( err ) {
 *                  console.log("Query failed.");
 *              } else {
 *                  console.log("Results: ");
 *                  console.log(res);
 *              }
 */

ZList.prototype.urls = function ( startDate, endDate, callback ) {

    var params = {};

    if ( typeof startDate !== 'undefined' && startDate !== null ) {
        params.start = startDate.toISOString();
    }

    if ( typeof endDate !== 'undefined' && endDate != null ) {
        params.end = endDate.toISOString();
    }

    var settings = this._settings;

    util.getURLWithParameters( ZLIST_QUERY_API_PATH, params, settings, function ( err, res ) {
        callback( err, res );
    });
};