'use strict';

var util = require('./util.js');
var request = require('superagent');
var logger = require('superagent-logger');

var WHOIS_QUERY_API_PATH = '/whois/query';

module.exports = Whois;

/**
 *
 *
 * Class to interact with RiskIQ whois API.  For details on this API, see:
 * https://sf.riskiq.net/crawlview/api/docs/controllers/WhoisController.html
 *
 * Created by jferg@riskiq.net.
 *
 * @class Whois
 * */

/**
 * Instantiate a whois API object using the given settings object for configuration.
 *
 * @param {Settings} settings A riq-api-js Settings object configured with the correct URL, api_key, and api_token.
 * @constructor
 */

function Whois( settings ) {
    this._settings = settings;
};

/**
 * Perform a whois query asynchronously against the RiskIQ API.
 *
 * @param {Object} criteria Set of criteria to query for.  Allowed criteria are domain, email address, nameServer, and raw text.
 * @param {Integer} maxResults Maximum number of results to return.  Capped at 1000.
 * @param {Function} callback Callback function.
 * @example whois.query( { domain: 'riskiq.com' }, 100, function (err, res ) {
 *              if ( err ) {
 *                  console.log("Query failed.");
 *              } else {
 *                  console.log("Results: ");
 *                  console.log(res);
 *              }
 */

Whois.prototype.query = function ( criteria, maxResults, callback ) {

    var qo = criteria;
    var maxResults = typeof maxResults !== 'undefined' ? maxResults : 100;
    qo.maxResults =  maxResults;
    util.postJSONToURL(WHOIS_QUERY_API_PATH, null, qo, this._settings, callback);

}
