/**
 * Created by justinferguson on 10/2/15.
 */

var path = require('path');
var node_url = require('url');
var querystring = require('querystring');
var request = require('superagent');
var logger = require('superagent-logger');

/**
 * Simple logger object that only logs to console.log.
 * @type {{debug: Function, info: Function, error: Function}}
 */

exports.simple_logger = {
    debug: function( message ) {
        console.log( message );
    },
    info: function( message ) {
        console.log(message);
    },
    error: function( message ) {
        console.log(message);
    }
};

/**
 * Build a full url out of a base and a variable number of path elements.
 *
 * @param {String} url The base URL to append the path elements to.
 * @param {String} .. The path elements to append to url.
 * @returns {String}
 */

exports.build_url = function () {
    if ( arguments.length === 1 ) return arguments[0];
    if ( arguments.length === 0 ) return null;

    var parsed = node_url.parse( arguments[0] );

    var args = Array.prototype.slice.call(arguments);

    var p = args.slice(1);

    p.unshift( parsed.pathname );

    parsed.pathname = path.join.apply( null, p );

    return parsed.format();
};

/**
 * Append a set of query parameters to a URL.
 *
 * @param {String} url The url to append the query parameters to.  May already contain query
 *                      parameters.
 * @param {Object} queryParameters An object containing the set of query parameters to append.
 * @returns {String}
 */

exports.append_query_parameters = function( url, queryParameters ) {
    if ( typeof url === 'undefined' || url === null ) { return null; };
    if ( typeof queryParameters !== 'object' || queryParameters === null ) { return url; }

    var parsed = node_url.parse( url, true );

    Object.keys(queryParameters).forEach( function( key, idx ) {
        // Do we already have a query string?
        if (!parsed.query) {
            // If not, let's create an object.
            parsed.query = {};
        }

        // Do we already have a value for this key?
        if (typeof parsed.query[key] === 'undefined') {
            // If not, make a new list.
            parsed.query[key] = new Array();
        }

        // If value exists and is not already an array, make it an array.
        if (! Array.isArray(parsed.query[key])) {
            parsed.query[key] = [parsed.query[key]];
        }

        if (! Array.isArray(queryParameters[key])) {
            parsed.query[key].push(queryParameters[key]);
        } else {
            queryParameters[key].forEach( function (value, idx) {
                parsed.query[key].push(value);
            });
        }
    });

    parsed.search = undefined;

    return parsed.format();
}

exports.getXMLURLWithParameters = function( path, queryParameters, settings, callback ) {

    var qurl = exports.build_url( settings.api_url_root, path );

    if ( typeof queryParameters !== 'undefined' ) {
        qurl = exports.append_query_parameters(qurl, queryParameters);
    }

    var agent = request
        .get(qurl)
        .buffer()
        .type('xml')
        .auth(settings.api_token, settings.api_key);

    if (settings.debug) {
        agent.use(logger({outgoing: true}));
    }

    agent.end(function(err,res) {
        if (err) {
            callback(err, res);
        } else{
            if ( res.statusCode === 204 ) {
                callback( null, { recordCount: 0, results: 0 } );
            } else {
                callback( null, res.text);
            }
        }
    });
}

exports.getURLWithParameters = function( path, queryParameters, settings, callback ) {

    var qurl = exports.build_url( settings.api_url_root, path );

    if ( typeof queryParameters !== 'undefined' ) {
        qurl = exports.append_query_parameters(qurl, queryParameters);
    }

    var agent = request
        .get(qurl)
        .auth(settings.api_token, settings.api_key);

    if (settings.debug) {
        agent.use(logger({outgoing: true}));
    }

    agent.end(function(err,res) {
        if (err) {
            callback(err, res);
        } else{
            if ( res.statusCode === 204 ) {
                callback( null, { recordCount: 0, results: 0 } );
            } if ( res.statusCode != 200 ) {
                callback( new Error( res.statusCode + " " + res.statusMessage + ": " + res.text ), res.body );
            } else {
                callback( null, res.body);
            }
        }
    });
};


exports.postJSONToURL = function (path, queryParameters, data, settings, callback) {

    var qurl = exports.build_url( settings.api_url_root, path );

    if ( typeof queryParameters !== 'undefined' && queryParameters !== null ) {
        qurl = exports.append_query_parameters(qurl, queryParameters);
    }

    var agent = request
        .post(qurl)
        .auth(settings.api_token, settings.api_key)
        .send(data);

    if (settings.debug) {
        agent.use(logger({outgoing: true}));
    }

    agent.end(function(err,res) {
        if (err) {
            callback(err, res);
        } else{
            if ( res.statusCode === 204 ) {
                callback( null, { recordCount: 0, results: 0 } );
            } else if ( res.statusCode !== 200 ) {
                callback( new Error( res.text ) );
            } else {
                callback( null, res.body);
            }
        }
    });
}
// filterBuilder 
exports.filterBuilder = {
    /*
    var testfilter = utils.fb.filter(utils.fb.field("assetType").equals("WEB_SITE"));
        testfilter2 = utils.fb.filter(utils.fb.field("assetType").equals("WEB_SITE")).or.filter(utils.fb.field("assetType").equals("ASN"));
        testfilter3 = utils.fb.filter(utils.fb.field("assetType").equals("WEB_SITE")).or.filter(utils.fb.field("assetType").equals("SSL_CERT"));
        testfilter4 = utils.fb.filter(testfilter2).and.filter(testfilter3);
    */

    filter: function( field ) {
        var obj = this; 
        if ( obj._nextFlag ) {
            if ( obj._nextFlag === 'or' ) {
                obj._nextFlag = null;
                if (obj._filter) {
                    obj._filter.filters[0].filters.push(field._field);
                } else {
                    // Sort of nonsensical to have (nextFlag == or) on an unset filter, but...
                    obj._filter = {
                        filters: [
                            {
                                filters: [field._field]
                            }
                        ]
                    }
                }
            } else {
                obj._filter = {
                    filters: [
                        {
                            filters: [field._field]
                        }
                    ]
                };
            }
        } else {
            obj._filter = {
                filters: [
                    {
                        filters: [field._field]
                    }
                ]
            };
        }
        
        return obj;
    },
    field: function( fieldname ) {
        var obj = this;
        obj._field = {
            field: fieldname,
        }
        return obj;
    },
    equals: function( fieldvalue ) {
        var obj = this;
        obj._field.value = fieldvalue;
        if ( obj._nextFlag === 'not' ) {
            obj._nextFlag = null;
            obj._field.type = "NE";
        } else {
            obj._field.type = "EQ";
        }
        return obj;
    },
    or: function() {
        var obj = this;
        obj._nextFlag = 'or';
        return obj;
    },
    not: function() {
        var obj = this;
        obj._nextFlag = 'not';
        return obj;
    },
    toString: function( ) {
        return JSON.stringify( this._filter );
    }
};

exports.fb = exports.filterBuilder;

