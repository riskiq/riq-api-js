'use strict';

var gulp    = require('gulp'),
    shell   = require('gulp-shell'),
    mocha   = require('gulp-mocha');

gulp.task('doc', [], shell.task([
    'node_modules/mr-doc/bin/mr-doc -s lib/ -o docs/riq-api-js -n "RiskIQ API for Node.js" --theme cayman'
]));

gulp.task('mocha', [], function () {
    return gulp.src('./test/**/*.test.js', { read: false })
        .pipe(mocha())
        .once('end', function () {});
});

gulp.task('docs', ['doc']);

gulp.task('test', ['mocha']);
