/**
 * Created by justinferguson on 10/1/15.
 *
 * index.js exports all submodules for the riskiq API.
 */

exports.Affiliate = require('./lib/affiliate.js');
exports.Whois = require('./lib/whois.js');
exports.Settings = require('./lib/settings.js');
exports.DNS = require('./lib/dns.js');
exports.Blacklist = require('./lib/blacklist.js');
exports.LandingPage = require('./lib/landingpage.js');
exports.Page = require('./lib/page.js');
exports.Event = require('./lib/event.js');
exports.ZList = require('./lib/zlist.js');
exports.Inventory = require('./lib/inventory.js');
exports.Mobile = {};
exports.Mobile.Incident = require('./lib/mobile/incident.js');
exports.Project = require('./lib/project.js');
